package servlets;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceAttachmentClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluencePageClient;
import com.atlassian.confluence.xmlrpc.client.api.domain.Attachment;
import com.atlassian.confluence.xmlrpc.client.api.domain.MutableAttachment;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Response;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import static com.atlassian.confluence.xmlrpc.client.api.domain.ConfluenceDomain.newAttachment;
import static com.atlassian.httpclient.api.EntityBuilders.newForm;

/**
 *
 */
@Named
public class MacroServlet extends HttpServlet
{
    private final HttpClient httpClient;
    private final ConfluenceAttachmentClient attachmentClient;
    private final ConfluencePageClient confluencePageClient;

    private static final long TEN_YEARS_SECONDS = 60L * 60L * 24L * 365L * 10L;
    private static final long TEN_YEARS_MILLISECONDS = 1000L * TEN_YEARS_SECONDS;

    @Inject
    public MacroServlet(
            @ComponentImport HttpClient httpClient,
            @ComponentImport ConfluenceAttachmentClient attachmentClient,
            @ComponentImport ConfluencePageClient confluencePageClient)
    {
        this.httpClient = httpClient;
        this.attachmentClient = attachmentClient;
        this.confluencePageClient = confluencePageClient;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");

        try
        {
            String content = renderContent(req.getParameterMap(), req.getParameter("body"));
            resp.setDateHeader("Expires", System.currentTimeMillis() + TEN_YEARS_MILLISECONDS);
            resp.setHeader("Cache-Control", "public");
            PrintWriter writer = resp.getWriter();
            writer.write(content);
            writer.close();
        }
        catch (UnknownHostException ex)
        {
            resp.sendError(500, "Unable to access " + ex.getMessage());
        }
        catch (IOException e)
        {
            resp.sendError(500, "Unable to access websequencediagrams.com");
        }
        catch (IllegalArgumentException e)
        {
            resp.sendError(400, "Unable to render diagram: " + e.getMessage());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String message = e.getMessage();
            if (e.getCause() != null)
            {
                message = e.getCause().getMessage();
            }
            resp.sendError(400, message);
        }
    }

    String renderContent(Map<String, String[]> params, String body) throws IOException
    {
        String format = params.get("format") != null ? params.get("format")[0] : "png";
        String style = params.get("style") != null ? params.get("style")[0] : "default";
        Response response = httpClient.newRequest(URI.create("http://www.websequencediagrams.com/index.php"))
                .setEntity(newForm()
                    .addParam("style", style)
                    .addParam("message", body)
                    .addParam("apiVersion", "1")
                    .addParam("format", format))
                .post().claim();

        String result = response.getEntity();
        String img;
        JSONObject data  = (JSONObject) JSONValue.parse(result);
        img = (String) data.get("img");
        JSONArray errors = (JSONArray) data.get("errors");
        if (!errors.isEmpty())
        {
            StringBuilder sb = new StringBuilder();
            for (String error : (List<String>) errors)
            {
                sb.append(error).append(", ");
            }
            sb.substring(0, sb.length() - 2);
            throw new IllegalArgumentException(sb.toString());
        }

        String key = params.get("key")[0];
        String file = params.get("file") != null ? params.get("file")[0] + "." + format: key + "." + format;
        Long pageId = Long.parseLong(params.get("ctx_page_id")[0]);

        boolean attachmentExists = false;
        for (Attachment attachment : confluencePageClient.getAttachments(pageId).claim())
        {
            if (key.equals(attachment.getComment()))
            {
                attachmentExists = true;
                break;
            }
        }

        if (!attachmentExists)
        {
            MutableAttachment toAttach = newAttachment();
            byte[] imageData = retrieveImageData("http://www.websequencediagrams.com/" + img);
            toAttach.setContentType("png".equals(format) ? "image/png" : "image/svg+xml");
            toAttach.setPageId(pageId);
            toAttach.setFileName(file);
            toAttach.setComment(key);
            attachmentClient.addAttachment(pageId, toAttach, imageData).claim();
        }
        return "<ac:image ac:title=\"" + file + "\"><ri:attachment ri:filename=\"" + file + "\" />" +
                "</ac:image>";
    }

    private byte[] retrieveImageData(String url) throws IOException
    {
        return IOUtils.toByteArray(httpClient.newRequest(
                URI.create(url)).get().claim().getEntityStream());
    }
}
